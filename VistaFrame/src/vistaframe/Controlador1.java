
package vistaframe;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador1 implements ActionListener
{
    private Vista1 v1;

    public Controlador1(Vista1 v1) {
        this.v1 = v1;
    }
public void Iniciar()
    {
    v1.setTitle("VISTA 1");
    v1.setLocationRelativeTo(null);
    v1.setVisible(true);
    asignarControl();
    
    }
    private void asignarControl(){
    v1.getAceptar().addActionListener(this);
    } 

    @Override
    public void actionPerformed(ActionEvent ae) {
                  EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                 //LANZAR EL CONTROLADOR
                 new Controlador2(
                 new Vista2(),
               v1.getNombre().getText(),
               v1.getTelefono().getText()
                 ).Iniciar();
            }
          });
                  v1.dispose();
    }
    
}
