
package vistaframe;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Controlador2 implements ActionListener{

    private String nombre, telefono;
    private Vista2 v2;    

    public Controlador2(Vista2 v2, String nombre, String telefono ) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.v2 = v2;
    }
    public void Iniciar(){
    v2.setVisible(true);
    v2.getNombre().setText(nombre);
    v2.getTelefono().setText(telefono);
    v2.setTitle("VISTA 2");
    v2.setLocationRelativeTo(null);
    
    asignarControl();
    
    }
    private void asignarControl(){
    v2.getCerrar().addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        System.exit(0);
    }
   
}
